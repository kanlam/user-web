/** @type {import('tailwindcss').Config} */
export default {
  darkMode: 'media',
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    fontFamily: {
      sans: ['Poppins', 'ui-sans-serif', 'system-ui'],
      display: ['Poppins', 'ui-sans-serif', 'system-ui'],
    },
    extend: {
      colors: {
        'arc-0': '#282b30',
        'arc-1': '#36393e',
        'arc-2': '#424549',
        'arc-blue': '#7289da',
        'dark-bg': '#1e2124',
      },
    },
  },
  plugins: [],
}
