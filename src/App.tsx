import { ToDo } from './page/to-do/ToDo'

function App() {
  return (
    <div className="min-h-screen bg-slate-100 dark:bg-dark-bg text-arc-0 dark:text-white">
      <ToDo />
    </div>
  )
}

export default App
