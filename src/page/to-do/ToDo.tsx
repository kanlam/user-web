import { Tasks } from './Tasks'

export function ToDo() {
  return (
    <div className="container mx-auto p-4">
      <input className="px-3 py-5 bg-slate-200 dark:bg-arc-0 w-full rounded shadow-xl" placeholder="What should I do ?" />

      <div className="pb-4" />

      <Tasks />
    </div>
  )
}
