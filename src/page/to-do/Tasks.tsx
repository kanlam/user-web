type ITask = {
  time: string
  text: string
  durations: string[]
}

export function Tasks() {
  const tasks: Array<ITask> = [
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
    {
      time: '12 PM',
      text: 'Working hard, making money',
      durations: ['12PM - 5PM'],
    },
  ]

  const renderTask = (task: ITask, i: number) => {
    return (
      <div className="flex py-4 items-center" key={`task_${i}`}>
        <div className="w-1/5">{task.time}</div>
        <div className="w-4/5 dark:bg-arc-0 bg-slate-200 p-4 rounded shadow-lg">{task.text}</div>
      </div>
    )
  }

  const taskView = tasks.map(renderTask)

  return (
    <div>
      <div>
        <h1 className="text-lg text-arc-blue font-medium">My tasks</h1>
        <div className="pb-2" />
        {taskView}
      </div>
    </div>
  )
}
